(function () {
    let entryFields = {
        "ignorator": [
            { name: "username", label: "username", options: { disabled: true } },
            { name: "userid", label: "userid", options: { disabled: true } },
            { name: "topicOnly", label: "topics only?", options: { type: "checkbox" } }
        ],
        "highlighter": [
            { name: "name", label: "username", options: { disabled: true } },
            { name: "userid", label: "userid", options: { disabled: true } },
            { name: "color", label: "color", options: { colorize: true } }
        ],
        "hiddenTopics": [
            { name: "name", label: "name", options: { disabled: true } },
            { name: "topicid", label: "topicid", options: { disabled: true } },
        ],
        "ignoratorKeywords": [
            { name: "userid", label: "keyword", options: { disabled: true } },
            { name: "casing", label: "ignore casing?", options: { type: "checkbox" } },
            { name: "regex", label: "is regex?", options: { type: "checkbox" } }
        ]
    }

    document.querySelectorAll('.tab').forEach((tab) => {
        tab.addEventListener('click', (event) => {
            event.target.parentElement.childNodes.forEach(child => {
                if (child.classList) {
                    child.classList.remove('selected');
                }
            });
            event.target.classList.add('selected');
            document.querySelectorAll('.tab-content').forEach(content => {
                content.style.display = 'none';
            });

            document.querySelector(`#tab_${event.target.textContent.replace(" ", "").replace("/", "")}`).style.display = null;
            serialize_settings().then(settings => {
                document.querySelector('#serialized_settings').value = settings;
            });
        })
    });

    document.querySelectorAll('input[type="checkbox"]').forEach((checkbox) => {
        doIfEnabled(checkbox.id, {}, () => {
            checkbox.checked = true;
        }, () => {
            checkbox.checked = false;
        });
        checkbox.addEventListener('click', (event) => {
            setFeature(checkbox.id, event.target.checked);
        });
    });

    document.querySelectorAll('input[type="text"],input[type="range"]').forEach(input => {
        getEnabledValue(input.id, (value) => input.value = value);
        input.addEventListener('change', (event) => {
            setFeature(input.id, event.target.value);
        });
    });

    document.querySelectorAll('.foxyeti-user-list').forEach((list) => {
        let name = list.id.substring(1);
        getSingletonFromStorage(name).then((wrapper) => {
            let inner = populateList(wrapper, name);
            list.appendChild(inner);
        });
    });

    document.querySelectorAll('.foxyeti-keywords-list').forEach((list) => {
        let name = list.id.substring(1);
        getSingletonFromStorage(name).then((wrapper) => {
            let inner = populateList(wrapper, name);
            list.appendChild(inner);
        });
    });

    document.querySelectorAll('.foxyeti-topic-list').forEach((list) => {
        let name = list.id.substring(1);
        getSingletonFromStorage(name).then((wrapper) => {
            let inner = populateList(wrapper, name);
            list.appendChild(inner);
        });
    });

    document.querySelectorAll('.foxyeti-flex-list').forEach(node => {
        let name = node.id.substring("footer_".length);
        getSingletonFromStorage(name).then((wrapper) => {
            let footer = makeFooter(entryFields[name], wrapper, name)
            node.appendChild(footer);
        });
    });

    document.querySelector("#twitter").addEventListener('click', (event) => {
        if (event.target.checked) {
            browser.permissions.request({ origins: ["*://*.twitter.com/*"] });
        }
    });

    document.querySelector("#apply_settings").addEventListener('click', (event) => {
        let wrapper = JSON.parse(document.querySelector("#serialized_settings").value);
        browser.storage.local.clear().then(() => {
            for (key of Object.keys(wrapper)) {
                syncSingleton(key, wrapper[key]);
            }
            window.location.reload(true);
        });
    })

    function serialize_settings() {
        return browser.permissions.getAll().then((currentPermissions) => {
            let settings = {}
            Object.assign(settings, singletons);
            settings["permissions"] = currentPermissions;
            settings["user_agent"] = navigator.userAgent;
            return settings;
        }).then(settings => {
            return JSON.stringify(settings);
        });
    }

    function populateList(wrapper, name) {
        let list = document.createElement('div');
        let header = makeHeaderRow(entryFields[name]);
        list.appendChild(header);

        if (Object.keys(wrapper[name]).length === 0) {
            warnEmptyRow(list);
            return list;
        }
        for (const [entryId, data] of Object.entries(wrapper[name])) {
            let row = makeRow(entryId, data, entryFields[name], (field, value) => {
                wrapper[name][entryId][field.name] = value;
                syncSingleton(name, wrapper);
            }, (key) => {
                delete wrapper[name][entryId]
                syncSingleton(name, wrapper, event => {
                    row.remove();
                    if (Object.keys(wrapper[name]).length === 0) {
                        warnEmptyRow(list);
                    }
                })
            });
            list.appendChild(row);
        }
        return list
    }

    function warnEmptyRow(table) {
        let empty = document.createTextNode(`There are no entries in this collection`);
        let row = document.createElement('div');
        row.appendChild(empty);
        table.appendChild(row);
        return;
    }

    function makeHeaderRow(entryFields) {
        let header = document.createElement('div');
        header.classList.add('foxyeti-user-entry', 'foxyeti-header');
        for (const field of entryFields) {
            let th = document.createElement('div');
            th.appendChild(document.createTextNode(field.label));
            header.appendChild(th);
        }
        let del = document.createElement('div');
        header.appendChild(del);
        return header;
    }

    function makeRow(key, data, entryFields, onEdit, onDelete) {
        let row = document.createElement('div');
        row.classList.add('foxyeti-user-entry');
        for (const field of entryFields) {
            let td = document.createElement('div');
            if (field.options && field.options.disabled) {
                td.appendChild(document.createTextNode(data[field.name]))
            } else {
                let input = document.createElement('input');
                input.type = 'text';
                if (field.options && field.options.type) {
                    input.type = field.options.type;
                    if (field.options.type === "checkbox") {
                        input.checked = data[field.name];
                        input.addEventListener('change', (event) => onEdit(field, event.target.checked));
                    }
                } else {
                    input.value = data[field.name];
                    if (field.options && field.options.colorize) {
                        row.style.backgroundColor = input.value;
                    }
                    input.addEventListener('input', (event) => {
                        onEdit(field, event.target.value);
                        if (field.options && field.options.colorize) {
                            row.style.backgroundColor = event.target.value;
                        }
                    });
                }
                td.appendChild(input);
            }
            row.appendChild(td);
        }

        let deletion = document.createElement('button')
        deletion.classList.add("browser-style", "text-right");
        deletion.appendChild(document.createTextNode('✖'))
        deletion.onclick = () => onDelete(key);
        let last = document.createElement('div');
        last.appendChild(deletion);
        row.appendChild(last);
        return row;
    }

    function makeFooter(entryFields, wrapper, name) {
        let footer = document.createElement('div');
        footer.classList.add('foxyeti-flex-entry');
        let entry = {};
        for (const field of entryFields) {
            let input = document.createElement('input');
            let div = document.createElement('div');
            input.addEventListener('input', (event) => {
                entry[field.name] = event.target.value;
            })
            input.type = field.options["type"] || "text";
            div.appendChild(input);
            footer.appendChild(div);
        }
        let save = document.createElement('button');
        save.classList.add('browser-style', 'default');
        save.appendChild(document.createTextNode('Add Entry'));
        save.onclick = () => {
            wrapper[name][entry['userid']] = entry;
            syncSingleton(name, wrapper, () => {
                let list = document.querySelector(`#_${name}`);
                let inner = populateList(wrapper, name);
                list.children[0].replaceWith(inner);
            });
            let replacement = makeFooter(entryFields, wrapper, name, key);
            footer.replaceWith(replacement);
        };

        footer.appendChild(save);
        return footer;
    }
})() 