(function () {
    document.querySelectorAll('input[type="checkbox"]').forEach((checkbox) => {
        doIfEnabled(checkbox.id, {}, () => {
            checkbox.checked = true;
        });
        checkbox.addEventListener('click', () => {
            setFeature(checkbox.id, checkbox.checked);
        });
    });
    document.querySelector('#foxyeti-options').addEventListener('click', (event) => {
        browser.runtime.openOptionsPage();
    });
})();