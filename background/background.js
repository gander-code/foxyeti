browser.runtime.onMessage.addListener((event) => {
    switch (event.type) {
        case 'notification':
            browser.notifications.create({
                "type": "basic",
                "iconUrl": browser.runtime.getURL("icons/foxyeti.png"),
                "title": event.title,
                "message": event.message
            })
            break;
        case 'ignorated':
            browser.tabs.query({ active: true, currentWindow: true }).then(
                tabs => {
                    browser.browserAction.setBadgeBackgroundColor({ color: "#2196F3", tabId: tabs[0].id });
                    browser.browserAction.setBadgeTextColor({ color: "#FFFFFF", tabId: tabs[0].id });
                    browser.browserAction.setBadgeText({
                        tabId: tabs[0].id,
                        text: "" + event.message
                    });
                }
            )
            break;
        case 'feature-set':
            if (['ignorator', 'highlighter'].includes(event.title)) {
                if (event.message) {
                    createMenu(event.title);
                } else {
                    removeMenu(event.title);
                }
            }
    }
});

function onContextClick(type, listener) {
    browser.tabs.query({ active: true, currentWindow: true })
        .then(tabs => {
            browser.tabs.sendMessage(tabs[0].id, { type: type, id: listener.targetElementId })
                .catch((error) => {
                    console.error(`error happened: ${error}`);
                });
        });
}

function createMenu(name) {
    console.log("creating menu");
    browser.menus.create({
        id: `${name}-add`,
        title: browser.i18n.getMessage(name),
        onclick: (listener) => onContextClick(name, listener),
        contexts: ['all'],
        documentUrlPatterns: ["*://*.endoftheinter.net/*"]
    });
}

function removeMenu(id) {
    browser.menus.remove(`${id}-add`)
}

doIfEnabled('ignorator', {}, () => {
    createMenu('ignorator')
}, {}, () => {
    removeMenu('ignorator')
});

doIfEnabled('highlighter', {}, () => {
    console.log('highlighter')
    createMenu('highlighter')
}, () => {
    console.log('no highlighter')
    removeMenu('highlighter')
});