(() => {
    let listeners = {};
    function addListener(key, onEvent) {
        listeners[key] = onEvent;
    }

    addListener('highlighter', onHighlight);
    addListener('ignorator', onIgnorate);
    function run() {
        let host = document.location.host;
        if (host.startsWith('wiki.')) {
            return
        }
        doIfEnabled('force_https', {}, forceHttps);
        document.addEventListener("DOMContentLoaded", () => {
            if (host.startsWith("fri.") || host.startsWith("m.")) {
                document.querySelectorAll('a[href]').forEach(link => {
                    link.addEventListener('click', () => {
                        setTimeout(() => runFeatures({ mobile: true, navigation: true }), 100);
                    }, true);
                })
                doIfEnabled('foxyeti_mobile', { mobile: true }, runFeatures)
            } else {
                runFeatures({});
            }
            console.log("if my calculations are correct, foxyeti is done loading.")
        });
        console.log('greetings. my name is jebuiz y\'har');
    }



    function runFeatures(options) {
        doOnDesktopIfEnabled('chair_guy', chairGuyRedirect);
        // version check is intentionally out of order
        doOnceIfEnabled('notify_version', options, checkVersion);
        doOnceIfEnabled('dramalinks', options, addDramaTicker);
        doOnDesktopIfEnabled('eastereggs', options, enableEasterEggs);
        doIfEnabled('show_history', options, showHistory);
        let path = document.location.pathname;
        if (path.startsWith('/showmessages.php')) {
            runMessageListFeatures(options);
        } else if (path.startsWith('/postmsg.php')) {
            runPostMessageFeatures(options);
        } else {
            runTopicListFeatures(options);
        }
    }

    function runMessageListFeatures(options) {
        runPostMessageFeatures(options);
        doIfEnabled('work_mode', options, applyWorkModeToMessageList);

        doIfEnabled('external_media', options, embedExternalMedia);
        doOnDesktopIfEnabled('fade_sigs', options, fadeSignatures);
        doOnDesktopIfEnabled('filter_me', options, addFilterMeLink);
        doIfEnabled('foxlinks', options, addFoxlinksQuotes);
        doIfEnabled('highlighter', options, highlightMessageListUsers);
        doIfEnabled('ignorator', options, ignorateMessageListUsers);
        doOnDesktopIfEnabled('img_resize', options, imgResize);
        doIfEnabled('like', options, addLikeButtons);
        doIfEnabled('logout_links', options, preventLogout);
        doIfEnabled('post_numbers', options, appendPostNumbers);
        doIfEnabled('quickpost_cache', options, cacheQuickpost);
        doIfEnabled('tc_tagging', options, tag_tc);
        doIfEnabled('tc_highlighter', options, highlight_tc);
        doOnDesktopIfEnabled('topic_img_map', options, addTopicImageMapLink);
        doIfEnabled('twitter', options, inlineTweets);
        doIfEnabled('tab_links', options, messageListTabLinks);
        doIfEnabled('user_notes', options, addUserNotes);
        doOnDesktopIfEnabled('viewer_count', options, live_readers);
        doIfEnabled('youtube', options, inlineYoutube);

        observeMessageListImageMutation(options);
        observeMessageListMutation(options);
    }

    function runPostMessageFeatures(options) {
        doOnDesktopIfEnabled('batch_uploads', options, replaceUploadForm);
        doOnDesktopIfEnabled('paste_upload', options, enable_paste_upload)
        doIfEnabled('shortcuts', options, addCtrlShortcuts);
        doOnDesktopIfEnabled('user_img_map', options, addUserImageMapLink);
    }

    function runTopicListFeatures(options) {
        doIfEnabled('work_mode', options, applyWorkmodeToTopicList);

        doIfEnabled('hidden_topics', options, runHiddenTopics);
        doIfEnabled('highlighter', options, highlightTopicListUsers);
        doIfEnabled('ignorator', options, ignorateTopicListUsers);
        doIfEnabled('ignorator', options, ignorate_keywords);
        doOnDesktopIfEnabled('last_page', options, linkToLastPage);
        doIfEnabled('tab_links', options, topicListTabLinks);
        doIfEnabled('topic_title_casing', options, titleCaseTopics);
        doOnDesktopIfEnabled('update_topiclist', options, update_topiclist);
        afterCompletion('ignorator', options, () => doIfEnabled('zebra_tables', zebraTables));
        observeTopicListMutation(options);
    }

    function observeMessageListMutation(options) {
        let observer = new MutationObserver((mutations) => {
            doIfEnabled('work_mode', options, applyWorkModeToMessageList);

            doIfEnabled('external_media', options, embedExternalMedia);
            doOnDesktopIfEnabled('fade_sigs', options, fadeSignatures);
            doIfEnabled('highlighter', options, highlightMessageListUsers);
            doIfEnabled('ignorator', options, ignorateMessageListUsers);
            doIfEnabled('img_resize', options, imgResize);
            doIfEnabled('like', options, addLikeButtons);
            doIfEnabled('logout_links', options, preventLogout);
            doIfEnabled('post_numbers', options, appendPostNumbers);
            doOnDesktopIfEnabled('scroll_on_post', options, scrollOnPost);
            doIfEnabled('tab_links', options, messageListTabLinks);
            doIfEnabled('tc_tagging', options, tag_tc);
            doIfEnabled('tc_highlighter', options, highlight_tc);
            doIfEnabled('twitter', options, inlineTweets);
            doIfEnabled('user_notes', options, addUserNotes);
            doIfEnabled('youtube', options, inlineYoutube);
        });
        let mutable = document.querySelector('#u0_1')
        if (options.mobile) {
            mutable = document.querySelector('main div')
        }
        observer.observe(mutable, { childList: true });
    }

    function observeTopicListMutation(options) {
        let totm = document.querySelector('td#totm');
        if (options.mobile) {
            totm = document.querySelector('main div');
        }
        if (totm != null) {
            let observer = new MutationObserver((mutations) => {
                doIfEnabled('work_mode', options, applyWorkmodeToTopicList);

                doIfEnabled('hidden_topics', options, runHiddenTopics);
                doIfEnabled('highlighter', options, highlightTopicListUsers);
                doIfEnabled('ignorator', options, ignorateTopicListUsers);
                doIfEnabled('ignorator', options, ignorate_keywords);
                doOnDesktopIfEnabled('last_page', options, linkToLastPage);
                doIfEnabled('topic_title_casing', options, titleCaseTopics);
            });
            observer.observe(totm, { childList: true });
        }
    }

    function observeMessageListImageMutation(options) {
        if (options.mobile) {
            return;
        }
        let observer = new MutationObserver((mutations) => {
            mutations.forEach(mutation => {
                const target = mutation.target;
                console.log(target)
                if (target.classList.contains('img-placeholder') || target.classList.contains('img-loaded')) {
                    doOnDesktopIfEnabled('thumbnail_img',options,  () => expandThumbnails(target));
                }
            })
        });
        document.querySelectorAll('.message').forEach(
            (target) => observer.observe(target, { childList: true, subtree: true })
        );
    }
    run();
    browser.runtime.onMessage.addListener((event) => {
        let target = browser.menus.getTargetElement(event.id);
        if (listeners[event.type]) {
            listeners[event.type](target);
        }
    });
})();
