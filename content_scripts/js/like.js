function addLikeButtons(options) {

    function addDesktopButton() {
        let user = document.querySelector('.userbar a').firstChild.textContent.replace(/ \(-?\d+\)$/i, '')
        document.querySelectorAll('.message-container > .message-top').forEach(
            (messageTop) => {
                let anchor = createLikeButton(messageTop, (event, poster) => {
                    event.target.previousElementSibling.onclick(event);
                    if (document.querySelector("h2 a[href='/topics/Anonymous']") !== null) {
                        user = "This Human";
                        poster = messageTop.childNodes[1].textContent;
                        poster = poster.substring(1, poster.length - 3);
                    }
                    like(user, poster);
                })
                anchor.classList.add('foxyeti-link');
                messageTop.appendChild(anchor);
            }
        )

        function like(user, poster) {
            let post_text = `\n<img src="http://i4.endoftheinter.net/i/n/f818de60196ad15c888b7f2140a77744/like.png" /> ${user} likes ${poster}'s post\n`;
            let textarea = document.querySelector('.quickpost-body textarea');
            let position = textarea.selectionEnd;
            textarea.value = textarea.value.substring(0, position) + post_text + textarea.value.substring(position);
            textarea.selectionEnd = position + post_text.length;
        }
    }

    function createLikeButton(messageTop, onclick) {
        if (messageTop.querySelector('.foxyeti-like') || messageTop.parentElement.classList.contains('quoted-message')) {
            return
        }
        messageTop.appendChild(document.createTextNode(' | '));
        let from = messageTop.firstChild;
        while(from.textContent.startsWith("From:") || from.textContent === " " || from.textContent == "$ $") {
            from = from.nextSibling;
        }
        let poster = from.textContent;
        let anchor = document.createElement('a');
        anchor.classList.add('foxyeti-like');
        anchor.onclick = (event) => onclick(event, poster);
        anchor.appendChild(document.createTextNode("Like"));
        return anchor;
    }

    function addMobileButton() {
        let user = document.querySelector("#_ div + div").firstChild.textContent
        document.querySelectorAll('section').forEach(section => {
            let top = section.querySelector('div');
            let anchor = createLikeButton(top, (event, poster) => {
                event.target.previousElementSibling.click(event);
                if (document.querySelector("h2 a[href='/topics/Anonymous']") !== null) {
                    user = "This Human";
                }
                like(user, poster);
            })
            top.appendChild(anchor);
        })

        function like(user, poster) {
            let post_text = `\n<img src="http://i4.endoftheinter.net/i/n/f818de60196ad15c888b7f2140a77744/like.png" /> ${user} likes ${poster}'s post\n`;
            let range = window.getSelection().getRangeAt(0);
            range.insertNode(document.createTextNode(post_text));
            range.collapse();
        }
    }

    if (options.mobile) {
        addMobileButton()
    } else {
        addDesktopButton();
    }
}

