let youtube_expr = /http(s)?:\/\/(www\.)?youtube.com\/watch\?v=.*/
let youtube_shortened_expr = /http(s)?:\/\/(www\.)?youtu.be\/.*/
function inlineYoutube() {
    document.querySelectorAll('a[href*="youtube.com"], a[href*="youtu.be"]').forEach((node) => {
        if (isBelowSig(node) || node.children.length > 1) {
            return;
        }
        if (youtube_expr.test(node.href)) {
            let index = node.href.indexOf("?v=") + 3;
            let videoid = node.href.substring(index)
            addNode(node, videoid)
        } else if (youtube_shortened_expr.test(node.href)) {
            let splits = node.href.split('/');
            addNode(node, splits[splits.length - 1]);
        }
    })

    function addNode(node, videoid) {
        getStorageValue('youtube-small').then(value => {
            let width = value ? 720 : 1280;
            let height = value ? 480 : 720;
            let frame = document.createElement('iframe');
            frame.src = `https://www.youtube.com/embed/${videoid}`
            frame.setAttribute('width', width);
            frame.setAttribute('height', height);
            node.appendChild(document.createElement('br'));
            node.appendChild(frame);
        });
    }
}
