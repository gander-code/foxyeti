const direct_source_image = /http(s)?:\/\/.*\.(jpg|jpeg|png|gif)$/i
const direct_source_video = /http(s)?:\/\/.*\.(webm|mp4|gifv)$/i
const imgur_gifv = /http(s)?:\/\/(.*\.)?imgur.com\/.*\.gifv$/i
const gfycat_hosted = /http(s)?:\/\/(.*\.)?gfycat.com\/[\w\d]+/i
const redgif_hosted = /http(s)?:\/\/(.*\.)?redgifs.com\/watch\/[\w\d]+/i

function embedExternalMedia(options) {
    document.querySelectorAll(".message a.l:not([imgsrc]):not(.foxyeti-media), section > div:nth-child(2) a[href]:not(.foxyeti-media)").forEach(
        (node) => {
            node.classList.add('foxyeti-media');
            if (isBelowSig(node)) {
                return;
            }
            let media = null;
            if (gfycat_hosted.test(node.href)) {
                embedGfycat(node);
                return
            } else if (redgif_hosted.test(node.href)) {
                embedRedgif(node);
                return
            } else if (direct_source_video.test(node.href)) {
                if (imgur_gifv.test(node.href)) {
                    node.href = node.href.substring(0, node.href.lastIndexOf(".gifv")) + ".mp4";
                }
                media = embedVideo(node);
            } else if (direct_source_image.test(node.href)) {
                media = embedImage(node);
            }
            if (media === null) {
                return;
            }
            let wrapper = document.createElement('div');
            wrapper.classList.add('foxyeti-fit-to-screen');
            wrapper.append(media);

            node.appendChild(wrapper);
        }
    )

    function isBelowSig(node) {
        let message_content = node.parentNode.textContent
        let sigIndex = message_content.lastIndexOf('---');
        let searchable = message_content.substring(0, sigIndex);
        return searchable.indexOf(node.href) < 0;
    }

    function embedImage(node) {
        let img = document.createElement('img');
        img.src = node.href;
        return img;
    }

    function embedVideo(node) {
        let split = node.href.split('.');
        let type_suf = split[split.length - 1];

        let source = document.createElement('source');
        source.src = node.href;
        source.type = `video/${type_suf}`;

        let video = document.createElement('video');
        video.controls = true;
        video.appendChild(source);
        return video;
    }

    function embedRedgif(node) {
        let media_id = node.href.split('.com/watch/')[1];
        fetchURL(`https://api.redgifs.com/v2/gifs/${media_id}`).then(result => {
            var response = JSON.parse(result);
            let video_source = document.createElement('source');
            video_source.src = response.gif.urls.hd;
            video_source.type = `video/mp4`;
            let video = document.createElement('video');
            video.controls = true;
            video.appendChild(video_source);

            let wrapper = document.createElement('div');
            node.classList.add('foxyeti-media');
            wrapper.classList.add('foxyeti-fit-to-screen');
            wrapper.appendChild(video);

            node.appendChild(wrapper);
        })
    }

    function embedGfycat(node) {
        let media_id = node.href.split('.com/')[1];
        fetchURL(`https://api.gfycat.com/v1/gfycats/${media_id}`).then(result => {
            var response = JSON.parse(result);
            let video_source = document.createElement('source');
            video_source.src = response.gfyItem.content_urls.webm.url;
            video_source.type = `video/webm`;
            let video = document.createElement('video');
            video.controls = true;
            video.appendChild(video_source);

            let wrapper = document.createElement('div');
            node.classList.add('foxyeti-media');
            wrapper.classList.add('foxyeti-fit-to-screen');
            wrapper.appendChild(video);

            node.appendChild(wrapper);
        })
    }
}

