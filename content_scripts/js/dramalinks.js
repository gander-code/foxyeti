function addDramaTicker(options) {
    function getDramalinks() {
        if (document.querySelector('.dramalinks')) {
            return
        }
        return fetchURL(
            'https://wiki.endoftheinter.net/api.php?action=parse&page=Dramalinks/current&format=json',
            {},
            (response) => response.json()
        ).then(response => {
            let drama = parseDrama(response);
            let items = drama.querySelectorAll('li')
            let dramalinks = document.createElement('div');
            dramalinks.classList.add('dramalinks');

            let ticker = document.createElement('span');
            ticker.style.padding = '5px';
            items.forEach(item => {
                let span = document.createElement('span');
                let iter = document.createNodeIterator(item, NodeFilter.SHOW_TEXT | NodeFilter.SHOW_ELEMENT)
                let node;
                while (node = iter.nextNode()) {
                    if (node.nodeType === 1 && node.tagName === "LI") {
                        continue
                    }
                    span.append(node);
                }

                ticker.append(document.createTextNode(' • '))
                ticker.append(span);
            });
            let background = "#FFF"
            if (drama.querySelector('font')) {
                background = drama.querySelector('font').getAttribute('color');
            } else {
                background = standardize_color(drama.querySelector('strong').style.color);
            }
            let textColor = determineTextColor(background);
            document.querySelector('head').append(styleText(textColor, background));
            dramalinks.append(ticker);

            return dramalinks;
        });
    }

    function setDramalinks_desktop(drama) {
        let menubar = document.querySelector('div.menubar')
        if (!menubar) {
            return
        }
        menubar.parentNode.insertBefore(drama, menubar.nextSibling);
    }

    function setDramalinks_mobile(drama) {
        drama.querySelectorAll("a[href]").forEach(
            anchor => anchor.href = anchor.href.replace("://boards.end", "://fri.end")
        )
        let pagebar = document.querySelector('main');
        pagebar.parentNode.insertBefore(drama, pagebar);
    }

    function standardize_color(str) {
        var ctx = document.createElement('canvas').getContext('2d');
        ctx.fillStyle = str;
        return ctx.fillStyle;
    }

    function parseDrama(drama) {
        let parser = new DOMParser();
        let dramalinks_html = drama.parse.text['*'].replaceAll("http://", "https://");
        return parser.parseFromString(dramalinks_html, 'text/html');
    }

    function determineTextColor(bgColor) {
        var color = (bgColor.charAt(0) === '#') ? bgColor.substring(1, 7) : bgColor;
        var r = parseInt(color.substring(0, 2), 16); // hexToR
        var g = parseInt(color.substring(2, 4), 16); // hexToG
        var b = parseInt(color.substring(4, 6), 16); // hexToB
        return (((r * 0.299) + (g * 0.587) + (b * 0.114)) > 186) ?
            '#000' : '#FFF';
    }

    function styleText(color, background) {
        let style = document.createElement("style");
        style.appendChild(document.createTextNode(
            `.dramalinks, .dramalinks a:any-link {
            font-weight: bold;
            padding: 2px;
            color: ${color} !important;
            background-color: ${background} !important;
            text-align: center;
        }`));
        return style;
    }
    getDramalinks(options).then(drama => {
        if (!options.mobile) {
            setDramalinks_desktop(drama)
        } else {
            setDramalinks_mobile(drama);
        }
    });

}