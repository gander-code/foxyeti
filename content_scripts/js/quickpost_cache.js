function cacheQuickpost() {
    let textarea = document.querySelector('.quickpost-body > textarea')
    textarea.addEventListener('input', updateCache);

    document.querySelector('form.quickpost input[type="submit"][name="post"]').addEventListener('click', (event) => {
        event.preventDefault();
        clearSingleton('quickpost');
    });
    const params = new URLSearchParams(window.location.search);
    const topic = params.get('topic');
    getSingletonFromStorage('quickpost').then(
        (wrapper) => {
            if (wrapper.quickpost[topic]) {
                textarea.value = wrapper.quickpost[topic];
            }
        }
    );

    function updateCache(event) {
        getSingletonFromStorage('quickpost').then(
            (wrapper) => {
                wrapper.quickpost[topic] = event.target.value;
                browser.storage.local.set(wrapper);
            }
        )
    }
}