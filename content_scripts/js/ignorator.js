function ignorateMessageListUsers(options) {
    function blur_post(messageTop) {
        messageTop.nextSibling.style = "filter: blur(8px); cursor: pointer";
        messageTop.nextSibling.onclick = () => {
            messageTop.classList.add('foxyeti-click');
            messageTop.nextSibling.style = "";
            count--;
            browser.runtime.sendMessage({
                type: 'ignorated',
                message: count,
            });
        }
    }

    getSingletonFromStorage('ignorator').then(
        (wrapper) => {
            let selector = options.mobile ? 'section > div:nth-child(1), div[data-msgid] > div' : '.message-top'
            let messages = document.querySelectorAll(selector);
            let count = 0;
            getStorageValue('blurry-ignorator').then(blur => {
                messages.forEach((messageTop) => {
                    if (shouldIgnorateMessage(options, wrapper, messageTop)) {
                        if (blur) {
                            blur_post(messageTop)
                        } else {
                            messageTop.parentElement.style = "display: none;";
                        }
                        count++;
                    }
                });
                if (count > 0) {
                    browser.runtime.sendMessage({
                        type: 'ignorated',
                        message: count,
                    });
                }
                onFeatureComplete('ignorator')
            });
        });
}

function ignorateTopicListUsers(options) {
    let count = 0;
    getSingletonFromStorage('ignorator').then(
        (wrapper) => {
            let selector = options.mobile ? 'table tr:not(:nth-child(1))' : '.grid tr:not(:nth-child(1))';
            document.querySelectorAll(selector).forEach(row => {
                if (shouldIgnorateTopic(options, wrapper, row)) {
                    row.style.display = 'none';
                    count++;
                }
            });
            if (count > 0) {
                browser.runtime.sendMessage({
                    type: 'ignorated',
                    message: count,
                });
            }
            onFeatureComplete('ignorator')
        }
    );
}

function shouldIgnorateTopic(options, wrapper, row) {
    let username = row.childNodes[1]
    if (options.mobile) {
        let should = Object.entries(wrapper.ignorator).filter(([_, value]) => {
            return value.username === username.textContent
        }).length != 0
        return should;
    }
    let tc = getTCId(row);
    return wrapper.ignorator[tc];
}

function shouldIgnorateMessage(options, wrapper, row) {
    if (options.mobile) {
        let username = row.firstChild;
        if (username.textContent === "From: ") {
            username = username.nextSibling;
        }
        return Object.entries(wrapper.ignorator).filter(([_, value]) => value.username === username.textContent).length != 0
    } else {
        let poster = row.querySelector('a[href*="user"]')
        if (poster && poster.href) {
            let userid = poster.href.substring(poster.href.indexOf("user=") + "user=".length);
            return wrapper.ignorator[userid];
        }
        return false;
    }
}

function onIgnorate(target) {
    let host = document.location.host;
    let options = {};
    if (host.startsWith('m.') || host.startsWith('fri.')) {
        options.mobile = true;
    }
    if (document.location.pathname.indexOf('/showmessages.php') === 0) {
        ignorateFromInsideTopic(target, options);
    } else {
        ignorateFromTopicList(target, options);
    }
}

function doIgnorate(node) {
    let poster = getPoster(node);
    let userid = getUserId(node);
    let selfid = getSelf();
    if (selfid === userid) {
        browser.runtime.sendMessage({
            type: 'notification',
            title: 'ignorator error!',
            message: "If you are annoying yourself, just log off bro"
        });
        return;
    }
    setIgnoratedUser(userid, poster.textContent);
}

function ignorateFromTopicList(target, options) {
    let row = getTableRow(target);
    if (row === null) {
        return;
    }
    doIgnorate(row);
}


function ignorateFromInsideTopic(target, options) {
    if (document.querySelector("h2 a[href='/topics/Anonymous']") !== null) {
        browser.runtime.sendMessage({
            type: 'notification',
            title: 'ignorator error!',
            message: "It is unwise to try to ignore anonymous users"
        });
        return;
    }
    let messageContainer = getMessageContainer(target);
    if (messageContainer === null) {
        return;
    }
    doIgnorate(messageContainer);
}

function getSelf() {
    let user = document.querySelector('.userbar a');
    return user.href.substring(user.href.indexOf("user=") + "user=".length);
}

function getTableRow(target) {
    if (target.classList.contains("body")) {
        return null;
    }
    if (target.nodeName == "TR") {
        return target;
    }
    return getTableRow(target.parentNode);
}

function getMessageContainer(target) {
    if (target.classList.contains("body")) {
        return null;
    }
    if (target.classList.contains("message-container")) {
        return target;
    }
    return getMessageContainer(target.parentNode);
}

function setIgnoratedUser(userid, name, topicOnly) {
    browser.runtime.sendMessage({
        type: 'notification',
        title: 'ignorator success!',
        message: `foxyeti will now ignore posts and topics from ${name} (userid: ${userid})`
    });
    getSingletonFromStorage('ignorator').then(
        (wrapper) => {
            wrapper.ignorator[userid] = { "username": name, "userid": userid, "topicOnly": topicOnly };
            syncSingleton('ignorator', wrapper, ignorateMessageListUsers);
        },
        onError
    )
}

function removeIgnoratedUser(userid) {
    getSingletonFromStorage('ignorator').then(
        (wrapper) => {
            delete wrapper.ignorator[userid]
            syncSingleton('ignorator', wrapper);
        },
        onError
    )
}
