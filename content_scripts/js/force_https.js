function forceHttps() {
    if (document.location.protocol != 'https:') {
        console.log('current protocol is not https... forcing');
        document.location.href = 'https:' + document.location.href.substring(document.location.protocol.length);
    }
}
