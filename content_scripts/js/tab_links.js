function topicListTabLinks() {
    document.querySelectorAll('table.grid a').forEach(anchor => {
        anchor.setAttribute("target", "_blank");
    });
}

function messageListTabLinks() {
    document.querySelectorAll('div#u0_1 a:not(.jump-arrow)').forEach(anchor => {
        anchor.setAttribute("target", "_blank");
    });
}
