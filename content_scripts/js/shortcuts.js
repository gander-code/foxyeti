function addCtrlShortcuts(options) {
    let editor = document.querySelector('textarea[name=message]');
    if (options.mobile) {
        editor = document.querySelector('div[contenteditable="true"]')
    }

    function getTag(event) {
        let tag = null;
        switch (event.key) {
            case 'b':
                tag = 'b'
                break;
            case 'i':
                tag = 'i'
                break;
            case 'm':
                tag = 'mod'
                break;
            case 'p':
                tag = 'pre'
                break;
            case 'q':
                tag = 'quote'
                break;
            case 's':
                tag = 'spoiler caption="spoiler"'
                break;
            case 'u':
                tag = 'u'
                break;
        }
        if (tag) {
            event.preventDefault();
        }
        return tag;
    }

    function updateMobileEditor(tag) {
        let selection = window.getSelection();
        let start_tag = document.createTextNode(`<${tag}>`);
        let end_tag = document.createTextNode(`</${tag.split(' ')[0]}>`)
        let range = selection.getRangeAt(0);
        let value = range.cloneContents();
        let collapse_inside = range.startOffset === range.endOffset;
        range.deleteContents();
        range.insertNode(end_tag);
        range.insertNode(value);
        range.insertNode(start_tag);
        if (collapse_inside) {
            selection.collapse(end_tag)
        } else {
            selection.collapseToEnd()
        }
    }

    function updateDesktopEditor(event, tag) {
        let start = event.target.selectionStart;
        let end = event.target.selectionEnd;
        let start_tag = `<${tag}>`
        let end_tag = `</${tag.split(' ')[0]}>`
        let selection_end = event.target.selectionEnd;
        let final_cursor = selection_end + start_tag.length;
        if (event.target.selectionStart !== event.target.selectionEnd) {
            final_cursor += end_tag.length;
        }
        inject(event.target, end_tag, end)
        inject(event.target, start_tag, start);
        event.target.selectionStart = final_cursor
        event.target.selectionEnd = final_cursor

        function inject(textarea, value, position) {
            textarea.value = textarea.value.substring(0, position) + value + textarea.value.substring(position);
        }
    }

    editor.addEventListener('keydown', (event) => {
        if (!event.ctrlKey) {
            return
        }
        let tag = getTag(event);
        if (!tag) {
            return
        }
        if (options.mobile) {
            updateMobileEditor(tag);
        } else {
            updateDesktopEditor(event, tag)
        }
    });
}
