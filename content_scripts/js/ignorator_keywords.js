function ignorate_keywords() {
    getSingletonFromStorage('ignoratorKeywords').then(wrapper => {
        document.querySelectorAll('.grid tr').forEach(row => {
            const name = getTopicName(row);
            if (name) {
                for (let key of Object.keys(wrapper.ignoratorKeywords)) {
                    let topicName = name;
                    if (wrapper.ignoratorKeywords[key].casing) {
                        topicName = topicName.toLowerCase();
                        key = key.toLowerCase()
                    }
                    if (wrapper.ignoratorKeywords[key].regex) {
                        if (topicName.search(key) >= 0) {
                            row.style.display = 'none';
                        }
                    } else if (topicName.split(" ").includes(key)) {
                        row.style.display = 'none';
                    }
                }
            }
        });
    });
}