let twitter_expr = /http(s)?:\/\/(www\.)?twitter.com\/.*/

let parser = new DOMParser();

function inlineTweets(options) {
    function changeTag(node, tag) {
        const clone = document.createElement(tag)
        for (const attr of node.attributes) {
            clone.setAttribute(attr.name, attr.value)
        }
        while (node.firstChild) {
            clone.appendChild(node.firstChild)
        }
        node.replaceWith(clone)
        return clone
    }

    document.querySelectorAll('a[href*="twitter.com"]:not(.no-embed)').forEach((node) => {
        if (isBelowSig(node) || node.parentNode.classList.contains('message-top') || ['P', 'BLOCKQUOTE'].includes(node.parentNode.nodeName)) {
            return;
        }
        node.setAttribute("target", "_blank");
        if (twitter_expr.test(node.href)) {
            fetchURL(`https://publish.twitter.com/oembed?dnt=true&omit_script=true&url=${encodeURIComponent(node.href)}`).then(result => {
                var response = JSON.parse(result);
                let tweet = parser.parseFromString(response.html, "text/html");
                let blockquote = changeTag(tweet.querySelector('blockquote'), 'div');

                blockquote.classList.add('quoted-message' );
                if (options.mobile) {
                    blockquote.classList.add('EaHLf')
                }

                let anchor = node.cloneNode(true)
                let tweet_top = document.createElement('div');
                tweet_top.classList.add('message-top');
                tweet_top.appendChild(anchor);
                node.replaceWith(blockquote);
                blockquote.prepend(tweet_top);
            });
        }
    });
}