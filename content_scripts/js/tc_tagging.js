function tag_tc() {
    const params = new URLSearchParams(window.location.search);
    const topic = params.get('topic');

    applyToTCPost(topic, doTagging);

    function doTagging(userid) {
        document.querySelectorAll('.message-top').forEach((row) => {
            if (row.querySelector('.foxyeti-tc')) {
                return
            }
            let poster = getUserId(row);
            if (userid === poster) {
                let span = document.createElement('span');
                span.classList.add('foxyeti-tc')
                let b = document.createElement('b');
                b.append(document.createTextNode('TC'));
                span.append(' | ')
                span.append(b);
                row.insertBefore(span, getPoster(row).nextSibling);
            }
        })
    }
}

async function applyToTCPost(topic, callback) {
    getSingletonFromStorage('topic_tc').then(
        (wrapper) => {
            if (!wrapper.topic_tc[topic]) {
                findTC(topic, wrapper, callback);
            } else {
                let userId = wrapper.topic_tc[topic];
                callback(userId)
            }
        }
    )

    function findTC(topic, wrapper, callback) {
        fetchURL(`https://boards.endoftheinter.net/showmessages.php?topic=${topic}`).then(response => {
            let pattern = /\<b\>From:\<\/b\>\s+\<a href="\/\/endoftheinter\.net\/profile\.php\?user=(\d+)"\>/m
            let m = pattern.exec(response);
            if (m) {
                syncSingleton('topic_tc', wrapper, () => {
                    callback(m[1]);
                });
            }
        });
    }
}