function zebraTables(options) {
    if (!document.querySelector('table')) {
        return
    }
    if (document.querySelector('.foxyeti-zebra-row')) {
        return
    }
    function styleText(color) {
        let style = document.createElement("style");
        style.appendChild(document.createTextNode(
            `.foxyeti-zebra-row {
                background-color: ${color} !important;
            }`
        ));
        document.querySelector('head').append(style);
    }

    function apply_desktop() {
        let td = document.querySelector(".grid td");
        if (!td) {
            return
        }
        getEnabledValue('zebra_tables_color',
            value => () => {
                return value;
            }, () => {
                let color = getComputedStyle(td).getPropertyValue('background-color');
                let amount = isColorBright(color) ? .95 /*darken since it's bright*/ : 1.05 /*brighten since it is dark*/;
                let zebraColor = brighten(color, amount);
                return zebraColor;
            }).then(value => {
                apply(value, ".grid tr:not(:first-child)")
            });
    }


    function apply_mobile() {
        let td = document.querySelector("main td");
        if (!td) {
            return
        }
        getEnabledValue('zebra_tables_color',
            value => () => {
                return value;
            }, () => {
                let color = getComputedStyle(td).getPropertyValue('background-color');
                let amount = isColorBright(color) ? .95 /*darken since it's bright*/ : 1.05 /*brighten since it is dark*/;
                let zebraColor = brighten(color, amount);
                return zebraColor;
            }).then(value => {
                apply(value, "main tr:not(:first-child)")
            });
    }

    function apply(color, selector) {
        styleText(color);
        let increment = 0;
        document.querySelectorAll(selector).forEach((node, index) => {
            if (node.style.display === 'none') {
                increment++
                return;
            }
            if ((increment+index) % 2 === 0) {
                node.querySelectorAll('td').forEach(td => {
                    td.classList.add('foxyeti-zebra-row')
                })
            }
        })
    }

    if (options.mobile) {
        apply_mobile()
    } else {
        apply_desktop()
    }
}