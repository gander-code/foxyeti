function titleCaseTopics(options) {
    if (options.mobile) {
        document.querySelectorAll('table tr > td a').forEach(anchor => anchor.textContent = toTitleCase(anchor.textContent));
    } else {
        document.querySelectorAll('.grid tr').forEach(row => setTopicName(row, toTitleCase(getTopicName(row))))
    } 
}

function toTitleCase(str) {
    if (!str) {
        return
    }
    return str.split(' ').map(function (word) {
        if (word.charAt(0).toUpperCase() === word.charAt(0)) {
            return (word.charAt(0).toUpperCase() + word.toLowerCase().slice(1));
        }
        return word.toLowerCase();
    }).join(' ');
}