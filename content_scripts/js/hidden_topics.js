function runHiddenTopics() {
    hideHiddenTopics();
    addHideTopicButton();
}


function addHideTopicButton() {
    document.querySelectorAll('.grid tr').forEach(row => {
        let tags = row.querySelector('.oh .fr');
        if (tags == null) {
            return
        }
        let anchor = document.createElement('a');
        anchor.appendChild(document.createTextNode('✖'));
        anchor.style.cursor = "pointer";
        tags.appendChild(anchor);
        anchor.addEventListener('click', (event) => {
            let name = getTopicName(row);
            let id = getTopicId(row);
            onTopicHidden(id, name);
        });
    })
}

function onTopicHidden(id, name) {
    getSingletonFromStorage('hiddenTopics').then(
        (wrapper) => {
            wrapper.hiddenTopics[id] = { "name": name, "topicid": id};
            syncSingleton('hiddenTopics', wrapper, hideHiddenTopics);
        },
        onError
    )
}

function hideHiddenTopics() {
    getSingletonFromStorage('hiddenTopics').then((wrapper) =>
        document.querySelectorAll('.grid tr').forEach(row => {
            let id = getTopicId(row)
            if (wrapper.hiddenTopics[id]) {
                row.style.display = 'none';
            }
        }
    ));
}