function highlight_tc() {

    const params = new URLSearchParams(window.location.search);
    const topic = params.get('topic');
    getEnabledValue('tc_highlighter_color', (value) => {
        applyToTCPost(topic, doHighlighting);

        function doHighlighting(userid) {
            document.querySelectorAll('.message-top').forEach((row) => {
                if (row.querySelector('.foxyeti-tc-highlight')) {
                    return
                }
                let poster = getUserId(row);
                if (userid !== poster) {
                    return
                }
                row.style = `background-color: ${value}`
            });
        }
    })
    
}