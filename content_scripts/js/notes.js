function addUserNotes() {
    if (document.querySelector("h2 a[href='/topics/Anonymous']") !== null) {
        return;
    }
    document.querySelectorAll('.message-top').forEach(
        (messageTop) => {
            if (messageTop.querySelector('.foxyeti-note') || messageTop.parentElement.classList.contains('quoted-message')) {
                return;
            }
            let div = document.createElement('div');
            div.style.display = 'none';
            messageTop.appendChild(document.createTextNode(' | '));
            let poster = getPoster(messageTop);
            let userid = getUserId(messageTop);

            getSingletonFromStorage('notes').then((wrapper) => {
                let textarea = document.createElement('textarea');
                textarea.classList.add('foxyeti-usernote');
                textarea.addEventListener('input', (event) => {
                    wrapper.notes[userid] = event.target.value;
                    browser.storage.local.set(wrapper);
                });
                if (wrapper.notes[userid]) {
                    textarea.value = wrapper.notes[userid];
                }
                div.appendChild(textarea);
            });

            let anchor = document.createElement('a');
            anchor.appendChild(document.createTextNode("Notes"));
            anchor.classList.add('foxyeti-note', 'foxyeti-link');
            anchor.onclick = (event) => {
                if (div.style.display === 'none') {
                    div.style.display = null;;
                } else {
                    div.style.display = 'none';
                }
            };
            messageTop.appendChild(anchor);
            messageTop.appendChild(div);
        });

}