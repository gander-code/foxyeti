function highlightMessageListUsers(options) {
    getSingletonFromStorage('highlighter').then(
        (wrapper) => {
            let selector = options.mobile ? 'section > div:nth-child(1), div[data-msgid] > div' : '.message-top'
            document.querySelectorAll(selector).forEach(
                (messageTop) => {
                    let entry = shouldHighlight(options, wrapper, messageTop)
                    if (entry) {
                        messageTop.style = `background-color: ${entry.color};`;
                    }
                }
            );
        }
    );
}

function highlightTopicListUsers(options) {
    getSingletonFromStorage('highlighter').then(
        (wrapper) => {
            let selector = options.mobile ? 'main table tr:not(:nth-child(1))' : '.grid tr:not(:nth-child(1))'
            document.querySelectorAll(selector).forEach(
                row => {
                    let entry = undefined;
                    if (options.mobile) {
                        entry = shouldHighlight_mobile_topic(wrapper, row);
                    } else {
                        entry = shouldHighlight(options, wrapper, row)
                    }
                    if (entry) {
                        row.querySelectorAll('td').forEach(cell => {
                            cell.style.backgroundColor = entry.color
                        });
                    }
                }
            )
        }
    );
}

function shouldHighlight_mobile_topic(wrapper, row) {
    let username = row.childNodes[1].textContent;
    let entries = Object.values(wrapper.highlighter)
        .filter(value => value.name === username);
    return entries.length > 0 ? entries[0] : false
}

function shouldHighlight(options, wrapper, row) {
    if (options.mobile) {
        let username = row.firstChild;
        if (username.textContent === "From: ") {
            username = username.nextSibling;
        }
        let entries = Object.values(wrapper.highlighter)
            .filter(value => value.name === username);
        return entries.length > 0 ? entries[0] : false
    } else {
        let poster = row.querySelector('a[href*="user"]')
        if (poster && poster.href) {
            let userid = poster.href.substring(poster.href.indexOf("user=") + "user=".length);
            return wrapper.highlighter[userid];
        }
        return false;
    }
}

function onHighlight(target) {
    if (document.location.pathname.indexOf('/showmessages.php') === 0) {
        highlightFromInsideTopic(target);
    } else {
        highlightFromTopicList(target);
    }
}

function highlightFromTopicList(target) {
    let row = getTableRow(target);
    if (row === null) {
        return;
    }
    doHighlight(row);
}

function highlightFromInsideTopic(target) {
    if (document.querySelector("h2 a[href='/topics/Anonymous']") !== null) {
        browser.runtime.sendMessage({
            type: 'notification',
            title: 'highlighter error',
            message: `It is unwise to try to highlight anonymous users`
        });
        return;
    }
    let messageContainer = getMessageContainer(target);
    if (messageContainer === null) {
        return;
    }
    doHighlight(messageContainer);
}


function doHighlight(node) {
    let poster = getPoster(node);
    let userid = getUserId(node);
    setHighlightedUsers(userid, poster.textContent);
}

function setHighlightedUsers(userid, name) {
    browser.runtime.sendMessage({
        type: 'notification',
        title: 'highlighter success!',
        message: `foxyeti will now highlight ${name} (userid: ${userid})`
    });

    getSingletonFromStorage('highlighter').then(
        (wrapper) => {
            getEnabledValue('highlighter_color', (color) => {
                wrapper.highlighter[userid] = { "name": name, "userid": userid, "color": color };
                syncSingleton('highlighter', wrapper, highlightMessageListUsers);
            }, () => {
                wrapper.highlighter[userid] = { "name": name, "userid": userid, "color": "#FFFF00" };
                syncSingleton('highlighter', wrapper, highlightMessageListUsers);
            });
        },
        onError
    )
}

function removeHighlightedUser(userid) {
    getSingletonFromStorage('highlighted').then(
        (wrapper) => {
            delete wrapper.highlighted[userid]
            syncSingleton('highlighter', highlightMessageListUsers);
        },
        onError
    )
}
