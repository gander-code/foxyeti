function checkVersion(options) {
    const current_version = browser.runtime.getManifest().version;

    function getNotification(mobile) {
        return getStorageValue('version').then(value => {
            if (value !== undefined && majorVersion(value) === majorVersion(current_version)) {
                return
            }
            let version_notification = document.createElement('div');
            version_notification.classList.add('foxyeti-version', 'infobar');

            let span = document.createElement('span');
            span.textContent = `a foxyeti update has added new features! (${mobile ? "tap" : "click"} to hide)`;

            let wrapper = document.createElement('span');
            wrapper.style.padding = '5px';
            wrapper.append(span);
            version_notification.append(wrapper);
            version_notification.addEventListener('click', () => {
                setFeature('version', current_version);
                version_notification.remove();
            })
            return version_notification;
        });
    }

    function majorVersion(version) {
        return version.substring(0, version.lastIndexOf('.'))
    }

    function setNotification(notification) {
        let menubar = document.querySelector('div.menubar')
        if (!menubar) {
            return
        }
       
        menubar.parentNode.insertBefore(notification, menubar.nextSibling);
    }

    function setNotification_mobile(notification) {
        let pagebar = document.querySelector('main');
        pagebar.parentNode.insertBefore(notification, pagebar);
    }

    function styleText(mobile) {
        let style = document.createElement("style");
        style.appendChild(document.createTextNode(`.foxyeti-version {
            font-weight: bold;
            padding: 2px;
            text-align: center;
            ${mobile ? "background: var(--background-2);" : ""}
        }`));
        document.querySelector('head').append(style);
    }

    styleText(options.mobile);
    getNotification(options.mobile).then(notification => {
        if (document.querySelector('.foxyeti-version')) {
            return
        }
        if (!notification) {
            return;
        }
        if (!options.mobile) {
            setNotification(notification);
        } else {
            setNotification_mobile(notification);
        }
    });
}