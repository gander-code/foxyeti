function showHistory() {
    let anchor = document.createElement('a');
    anchor.href = '//boards.endoftheinter.net/history.php';
    anchor.text = 'Message History';
    let menubar = document.querySelector('.menubar');
    let logout = document.querySelector('.menubar a:nth-child(6)');
    menubar.insertBefore(anchor, logout);
    menubar.insertBefore(document.createTextNode(' | '), logout);
}
