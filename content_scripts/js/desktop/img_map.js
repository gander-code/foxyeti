function addTopicImageMapLink() {
    let topic = getTopicIdFromURL();
    let anchor = document.createElement('a');
    anchor.target = "_blank";
    anchor.href = `//images.endoftheinter.net/imagemap.php?topic=${topic}`;
    anchor.appendChild(document.createTextNode('Topic Image Map'));
    let span = document.createElement('span');
    span.appendChild(document.createTextNode(' | '));
    span.appendChild(anchor);
    let infobar = document.querySelector('#u0_2.infobar')
    if (infobar) {
        infobar.appendChild(span);
    }
}

function addUserImageMapLink() {
    let anchor = document.createElement('a')
    anchor.href = `//images.endoftheinter.net/imagemap.php`;
    anchor.target = "_blank";
    let btn = document.createElement('input');
    btn.type = 'button';
    btn.value = "My Image Map";
    anchor.appendChild(btn);
    let body = document.querySelector('.quickpost-body') ?? document.querySelector('#buttons');
    body.appendChild(anchor);
}