function scrollOnPost() {
    if (!hasNextPage()) {
        let messages = document.querySelector('#u0_1');
        let last = messages.children[messages.children.length - 1];
        let previousLast = messages.children[messages.children.length - 2];
        if (isBottomInView(previousLast)) {
            last.scrollIntoView({ behavior: "smooth" });
        }
    }
}

function hasNextPage() {
    var style = window.getComputedStyle(document.getElementById('nextpage'));
    return (style.display !== 'none')
}

function isBottomInView(el) {
    return (el.getBoundingClientRect().bottom <= window.innerHeight);
}

