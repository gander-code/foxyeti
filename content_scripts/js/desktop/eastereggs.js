function enableEasterEggs(options) {
    doIfEnabled('tag-eastereggs', options, () => {
        subtitles();
        japan();
    });
    march();
    raptorize();
    aReminder();
}