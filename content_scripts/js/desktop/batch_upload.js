function replaceUploadForm() {
    let topicId = new URLSearchParams(window.location.search).get('topic');
    function onUploadClick(mutations) {
        if (mutations[0].attributeName === 'class') {
            document.querySelector('iframe.upload_form').classList.add('foxyeti-uploads');
            replaceUploader(topicId);
        }
    }

    function onPaste(event) {
        let files = event.clipboardData.files;
        if (files.length > 0 && known_uploads.length === 0) {
            upload(undefined, undefined);
        }
        for (i = 0; i < files.length; i++) {
            let file = files[i];
            upload(file, file.name);
        }
    }

    function dragArea() {
        let area = document.createElement("div");
        area.classList.add("foxyeti-upload-drag");
        let p = document.createElement("p");
        p.appendChild(document.createTextNode("drag/paste images"));
        area.appendChild(p);
        area.addEventListener("dragover", (event) => {
            event.preventDefault();
        });
        area.addEventListener("drop", (event) => {
            event.stopPropagation();
            event.preventDefault();
            for (i = 0; i < event.dataTransfer.items.length; i++) {
                if (event.dataTransfer.items[i].kind !== "file") {
                    continue;
                }
                let file = event.dataTransfer.items[i].getAsFile();
                upload(file, file.name);
            }
        }, false)
        return area;
    }

    function replaceUploader(topicId) {
        fetchURL(`https://u.endoftheinter.net/u.php?topic=${topicId}`)
            .then(response => setUploader(response))
            .catch(console.error);
    }

    function setUploader(html) {
        let uploader = document.createElement('div');
        uploader.classList.add('upload_form', 'foxyeti-uploads');
        uploader.style = "width:340px;overflow-y:scroll;";
        uploader.append(dragArea());
        let form = document.createElement('form');
        form.enctype = "multipart/form-data";
        form.action = "/u.php"
        form.method = "POST";
        let files = document.createElement('input');
        files.name = "file";
        files.type = "file"
        files.multiple = true;
        form.appendChild(files);
        let submit = document.createElement('input')
        submit.type = 'submit';
        submit.value = "Upload File(s)";
        submit.addEventListener('click', (e) => {
            e.preventDefault()
            for (i = 0; i < files.files.length; i++) {
                let file = files.files[i];
                upload(file, file.name);
            }
        });
        form.appendChild(submit);
        uploader.appendChild(form);

        let parser = new DOMParser();
        let uploaded = parser.parseFromString(html, "text/html");
        uploaded.querySelectorAll('div.img').forEach(node => {
            uploader.append(node);       
            let input = node.querySelector('input')
            if (input.hasAttribute('data-uploaded')) {
                addImagesToPost(input.value);
            }
        });
        let previous_imgs = document.querySelectorAll('.foxyeti-uploads div.img');
        let old_uploader = document.querySelector('.upload_form.foxyeti-uploads');
        previous_imgs.forEach(image => uploader.appendChild(image));
        old_uploader.replaceWith(uploader);
    }

    function upload(data, filename, topicId, callback = () => { }) {
        let formData = new FormData();
        if (data !== undefined) {
            formData.append('file', data, filename);
        }
        fetchURL(`https://u.endoftheinter.net/u.php?topic=${topicId}`, {
            method: 'POST',
            body: formData,
        }).then(response => setUploader(response, callback))
    }

    function addImagesToPost(image) {
        let textarea = document.querySelector('textarea[name="message"]');
        let position = textarea.selectionEnd;
        let post_text = `${image}\n\n`;
        textarea.value = textarea.value.substring(0, position) + post_text + textarea.value.substring(position);
        textarea.selectionEnd = position + post_text.length;
    }

    var known_uploads = [];
    let canvas = document.querySelector('div.quickpost-canvas') ?? document.querySelector('form[action="postmsg.php"]');;
    canvas.addEventListener('paste', onPaste);
    const observer = new MutationObserver(onUploadClick);
    observer.observe(canvas, { attributes: true });
}