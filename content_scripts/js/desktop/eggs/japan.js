function japan() {
    window.addEventListener('load', () => {
        if (!document.querySelector("h2 a[href='/topics/Japan']")) {
            return;
        }
        let border = getComputedStyle(document.querySelector('table.message-body td.userpic')).getPropertyValue('border-left-color');
        let style = document.createElement("style");
        style.appendChild(document.createTextNode(
            `table.message-body {
                direction: rtl;
            }
            table.message-body td.userpic {
                border-left: 0px;
                border-right: 2px solid ${border};
            }
            table.message-body td.message {
                direction: ltr;
            }`
        ));
        document.querySelector('head').appendChild(style);
    });
}