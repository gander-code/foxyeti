function march() {
    const d = new Date();
    if (d.getMonth() !== 2 || d.getDate() > 7) {
        return
    }
    let style = document.createElement("style");
    style.appendChild(document.createTextNode(
        `* {
            text-transform:uppercase;
        }`
    ));
    document.querySelector('head').appendChild(style);
}