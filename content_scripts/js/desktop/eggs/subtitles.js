
function subtitles() {
    let wrapper = createWrapper();
    window.addEventListener('load', () => {
        if (document.querySelector("h2 a[href='/topics/Subtitles']")) {
            let content = [];
            document.querySelectorAll('.message-container').forEach(node => {
                let poster = node.querySelector('.message-top').children[1].textContent;
                let post = node.querySelector('.message').textContent;
                post = post.substring(0, post.lastIndexOf('---\n'));
                if (post.trim().length == 0) {
                    content.push(`${poster}: {blankpost}`);
                } else {
                    content.push(...split(`${poster}: ${post}`));
                }
            });
            content.push("", "")
            content = caption(content);
            setInterval(() => {
                content = caption(content);
            }, 2000);
        }
    });

    function split(content) {
        function split_length(content, size) {
            if (content.length === 0) {
                return []
            }
            if (content.length <= size) {
                return [content.trim()];
            }
            let subsection = [content.substring(0, size).trim()];
            let second = split_length(content.substring(size), size);
            subsection.push(...second);
            return subsection;
        }
        let sections = [];
        for (line of content.split("\n")) {
            let subsections = split_length(line, 50);
            sections.push(...subsections);
        }
        return sections;
    }

    function createWrapper() {
        let wrapper = document.createElement('div')
        wrapper.classList.add('foxyeti-subtitle-wrapper');
        document.querySelector('body').appendChild(wrapper);
        wrapper.appendChild(document.createTextNode(""));
        wrapper.appendChild(document.createTextNode(""));
        return wrapper;
    }

    function caption(content) {
        if (content.length > 0) {
            let message = content.shift();
            let p = document.createElement('p');
            p.classList.add('foxyeti-subtitle-line');
            p.appendChild(document.createTextNode(message));
            wrapper.firstChild.remove();
            wrapper.appendChild(p);
        }
        return content;
    }
}