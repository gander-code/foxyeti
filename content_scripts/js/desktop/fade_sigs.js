function fadeSignatures() {
  getEnabledValue('fade_sigs_amount', (value) => {
    let style = document.createElement("style");
    style.appendChild(document.createTextNode(
      `.foxyeti-faded-sig {
            opacity: ${(100 - value) / 100};
            margin: 0;
        }`
    ));
    document.querySelector("head").appendChild(style);
  }, () => {
    let style = document.createElement("style");
    style.appendChild(document.createTextNode(
      `.foxyeti-faded-sig {
            opacity: 0.5;
            margin: 0;
        }`
    ));
    document.querySelector("head").appendChild(style);
  });

  document.querySelectorAll('div.message-container').forEach(container => {
    container.querySelectorAll('.message').forEach((node) => {
      const sig = indexOfSig(node);
      if (sig >= 0) {
        const faded_sig = document.createElement('p')
        faded_sig.classList.add('foxyeti-faded-sig');
        let remove = []
        node.childNodes.forEach((child, index) => {
          if (index >= sig) {
            faded_sig.appendChild(child.cloneNode(true));
            remove.push(child);
          }
        });
        remove.forEach(removal => {
          node.removeChild(removal);
        })
        node.append(faded_sig);
        container.querySelector('a[href*="quote="]').onclick = (event) => {
          event.preventDefault();
          faded_sig.remove();
          window.wrappedJSObject.QuickPost.publish('quote', event.target);
          node.append(faded_sig);
        }
      }
    })
  })

  function indexOfSig(node) {
    let last_index = -1;
    node.childNodes.forEach((child, index) => {
      if (child.textContent === "\n---") {
        last_index = index;
      }
    });
    return last_index;
  }
}