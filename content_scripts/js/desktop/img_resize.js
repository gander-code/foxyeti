function imgResize() {
    let style_src_width_only = `
    .message img {
        height: auto !important;
        width: auto !important;
        max-width: 100% !important;
    }
    .message .imgs {
        height: auto !important;
        width: auto !important;
        max-width: calc(100vw - 160px) !important;
    }
    span.img-placeholder, span.img-placeholder, span.img-loaded {
        min-height: 1px;
        min-width: 1px;
    }
    `
    let style_src_include_height = `
    .message img {
        height: auto !important;
        width: auto !important;
        max-width: 100% !important;
        max-height: calc(100vh - 160px) !important;
    }
    .message .imgs, {
        height: auto !important;
        width: auto !important;
        max-width: 100% !important;
        max-height: calc(100vh - 160px) !important;
    }
    span.img-placeholder, span.img-placeholder, span.img-loaded {
        min-height: 1px;
        min-width: 1px;
    }`
    doIfEnabled('img_resize_height', () => {
        let style = document.createElement("style");
        style.appendChild(document.createTextNode(style_src_include_height));
        document.querySelector("head").appendChild(style);
        resizeAllPlaceholders();
    }, () => {
        let style = document.createElement("style");
        style.appendChild(document.createTextNode(style_src_width_only));
        document.querySelector("head").appendChild(style);
        resizeWidthPlaceholders();
    })
}

function resizeWidthPlaceholders() {
    document.querySelectorAll('.message .img-placeholder').forEach(node => {
        let style = getComputedStyle(node);
        let img_width = style.width.slice(0, -2);
        let img_height = style.height.slice(0, -2);
        let max_width = window.innerWidth - 360;
        if (img_width > max_width) {
            let ratio = max_width / img_width;
            img_width = ratio * img_width;
            img_height = ratio * img_height;
            node.setAttribute('style', `width:${img_width}px;height:${img_height}px;`)
        }
    });
}

function resizeAllPlaceholders() {
    resizeWidthPlaceholders()
    let max_height = window.innerHeight - 160;
    document.querySelectorAll('.message .img-placeholder').forEach(node => {
        let style = getComputedStyle(node);
        let img_width = style.width.slice(0, -2);
        let img_height = style.height.slice(0, -2);
        if (img_height > max_height) {
            let ratio = max_height / img_height;
            img_width = ratio * img_width;
            img_height = ratio * img_height;
            node.setAttribute('style', `width:${img_width}px;height:${img_height}px;`)
        }
    });

}
