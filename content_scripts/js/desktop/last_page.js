function linkToLastPage() {
    getStorageValue('last_page_right').then(float_right => {
        document.querySelectorAll('.grid tr').forEach(
            row => {
                let total = getTopicNumMessages(row)
                if (!total) {
                    return
                }
                let pages = Math.ceil(total / 50);
                let anchor = document.createElement('a');
                anchor.title = "last page";
                anchor.href = `${row.querySelector('a').href}&page=${pages}`;
                anchor.appendChild(document.createTextNode("⯈"));
                anchor.style.textDecoration = 'none';
                if (float_right) {
                    anchor.classList.add("fr");
                }
                row.querySelector('td + td + td').appendChild(anchor);
            });
    });
}