function update_topiclist() {
    function listen() {
        let topics = get_topic_ids().toString();
        let now = Math.floor(Date.now() / 1000) * 1000;

        streamFetchURL(`https://fri.endoftheinter.net/api/watch-topics?topics=[${topics}]&_=${now}`, {}, (data) => {
            let rows = data.split("\n");
            if (rows[0].indexOf("id:") === 0) {
                try {
                    let event = JSON.parse(rows[1].substr("data:".length));
                    handle_event(event);
                } catch (error) {
                    console.log("error updating topiclist post counts:", error)
                }
            }
        });
    }

    function text_content(node) {
        return Array.prototype.filter
            .call(node.childNodes, e => e.nodeType === Node.TEXT_NODE)
            .map(e => e.textContent).join('');
    }

    function replace_text(node, text) {
        node.childNodes[0].replaceWith(document.createTextNode(text));
    }

    function handle_event(event) {
        let row = document.querySelector(`.oh a[href*="topic=${event.id}"]`).parentNode.parentNode.parentNode;
        let message_count = row.querySelector('td:nth-child(3)');

        let unread_messages = message_count.querySelector("span:not(.foxyeti-unread):not(.foxyeti-unread-wrapper)");

        let new_unread_messages = message_count.querySelector('span.foxyeti-unread');
        if (new_unread_messages === null) {
            new_unread_messages = document.createElement("span");
            new_unread_messages.classList.add("foxyeti-unread");
            new_unread_messages.appendChild(document.createTextNode("0"));
            let wrapper = document.createElement("span");
            wrapper.classList.add('foxyeti-unread-wrapper')
            wrapper.appendChild(document.createTextNode("["));
            wrapper.appendChild(new_unread_messages);
            wrapper.appendChild(document.createTextNode("] "));
            if (unread_messages === null) {
                wrapper.prepend(document.createTextNode(" "));
                message_count.appendChild(wrapper);
            } else {
                unread_messages.insertBefore(wrapper, unread_messages.lastChild);
            }
        }

        let num_messages = text_content(message_count);
        let diff = event.messageCount - num_messages;
        let latest_unread = Number.parseInt(new_unread_messages.textContent) + diff;
        let last_post = row.querySelector('td:nth-child(4)');
        let latest = new Date(event.timeLastPost);
        if (unread_messages !== null) {
            let anchor = unread_messages.querySelector('a[href*="#m"]');
            let num_unread = text_content(anchor);
            let latest_unread = Number.parseInt(num_unread.substr(1)) + diff;
            replace_text(anchor, `+${latest_unread}`);
        }
        replace_text(message_count, event.messageCount);
        replace_text(new_unread_messages, latest_unread);
        replace_text(last_post, `${latest.toLocaleDateString()} ${('0' + latest.getHours()).slice(-2)}:${('0' + latest.getMinutes()).slice(-2)}`);
    }
    listen();
}