function addFilterMeLink(options) {
    function filter_me() {
        let self = getSelf();
        let topic = getTopic();

        let anchor = getAnchor(self, topic);
        if (!anchor || !anchor.href) {
            return
        }

        doIfEnabled('filter_me_menu', options, () => {
            let userbar = document.querySelector('div.userbar')
            userbar.append(document.createTextNode(' | '))
            userbar.append(anchor);
        }, () => {
            let span = document.createElement('span');
            span.appendChild(document.createTextNode(' | '));
            span.appendChild(anchor);
            document.querySelector('#u0_2').appendChild(span);
        })
    }

    function getAnchor(self, topic, options) {
        let anchor = document.createElement('a');
        if (document.querySelector("h2 a[href='/topics/Anonymous']") !== null) {
            anchor.href = document.querySelector('.quickpost-body a[href*="&u="]').href;
        } else {
            anchor.href = `/showmessages.php?topic=${topic}&u=${self}`;
        }
        anchor.appendChild(document.createTextNode('Filter Me'));
        return anchor;
    }

    function getSelf() {
        let user = document.querySelector('.userbar a');
        return user.href.substring(user.href.indexOf("user=") + "user=".length);
    }

    function getTopic() {
        const params = new URLSearchParams(window.location.search);
        return params.get('topic');
    }

    filter_me(options);
}