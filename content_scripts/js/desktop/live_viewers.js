function live_readers() {
    function get_last_post(topic) {
        let now = Math.floor(Date.now() / 1000) * 1000;
        var bookmarkResolve, bookmarkReject;
        const bookmark = new Promise((resolve, reject) => {
            bookmarkResolve = resolve;
            bookmarkReject = reject;
        });
        streamFetchURL(`https://fri.endoftheinter.net/api/watch-topics?topics=[${topic}]&_=${now}`, {}, (data) => {
            let rows = data.split("\n");
            for (row of rows) {
                if (row.indexOf("data:") === 0 && row.length > "data:".length) {
                    try {
                        let event = JSON.parse(row.substr("data:".length));
                        if (event.type === "bookmark") {
                            bookmarkResolve(event.bookmarks);
                            return event.bookmarks;
                        }
                    } catch (error) {
                        console.log("error updating messagelist viewer counts:", error)
                    }
                }
            }
        })
        return bookmark;
    }

    function handle_event(event, close) {
        switch (event.type) {
            case "viewers": {
                document.querySelector(".foxyeti-viewer-count").textContent = event.count;
                if (event.quip) {
                    document.querySelector(".foxyeti-viewer-quip").textContent = ` people ${event.quip}`;
                }
                break;
            }
            case "message": {
                if (last_post !== event.message.id) {
                    close();
                    replace_infobar();
                    listen();
                }
                last_post = event.message.id;
                break;
            }
        }
    }

    function listen() {
        const topic = getTopicIdFromURL();
        get_last_post(topic).then(value => {
            const latest = value[topic].id;
            streamFetchURL(`https://fri.endoftheinter.net/api/topic/${topic}/subscribe?_=${latest}`, {}, (data) => {
                let rows = data.split("\n");
                for (row of rows) {
                    if (row.indexOf("data:") === 0 && row.length > "data:".length) {
                        try {
                            let event = JSON.parse(row.substr("data:".length));
                            handle_event(event, close);
                        } catch (error) {
                            console.log("error updating messagelist viewer counts:", error)
                        }
                    }
                }
            });
        });
    }

    function replace_infobar() {
        let infobar = document.querySelector("#u0_4.infobar");
        if (!infobar) {
            return;
        }
        let original_content = infobar.textContent;
        let count_index = original_content.search(/\d+/);
        let prefix = original_content.substring(0, count_index);
        let post_prefix = original_content.substr(count_index);
        let count = post_prefix.split(" ", 1)[0];
        let suffix = original_content.substr(count_index + count.length);
        let span = document.createElement("span");
        span.classList.add("foxyeti-viewer-count");
        span.appendChild(document.createTextNode(count));
        let quip = document.createElement("span");
        quip.classList.add("foxyeti-viewer-quip");
        quip.appendChild(document.createTextNode(suffix));

        let replacement = document.createElement('div');
        replacement.id = "u0_4";
        replacement.classList.add("infobar");
        replacement.appendChild(document.createTextNode(prefix));
        replacement.appendChild(span);
        replacement.appendChild(quip);
        infobar.replaceWith(replacement);
    }
    replace_infobar();
    listen();
}