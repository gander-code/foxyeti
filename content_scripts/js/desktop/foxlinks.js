function addFoxlinksQuotes() {
    let top = document.querySelector('.message-top');
    if (!top) {
        return
    }
    let color = getComputedStyle(top).getPropertyValue('background-color');
    let css_style = document.createElement('style');
    css_style.appendChild(document.createTextNode(
        `.quoted-message, EaHLf { 
            border:${color} 2px solid; 
            margin: 0 30px 2px 30px; 
            border-radius: 5px; 
            padding-left: 3px; 
        }
        .quoted-message .message-top { 
            background-color: ${color}; 
            margin-top: -2px !important; 
            margin-left: -3px !important; 
            border-radius: 3px 3px 0 0; 
        }`
    ));

    document.querySelector('head').appendChild(css_style);
}
