function expandThumbnails(node) {
    node.querySelectorAll('img[src*="/i/n/"]').forEach(
        img => {
            let original = img.src;
            let replacement = img.src.replace("/i/n/", "/i/t/").replace(/.{3,5}$/, ".jpg");

            img.parentNode.parentNode.onclick = (event) => {
                event.preventDefault();
                if (event.shiftKey) {
                    window.open(img.parentNode.parentNode.href, "_blank");
                } else {
                    img.parentNode.removeAttribute('style');
                    replaceImage(img, replacement, original);
                }
            }
        }
    );

    node.querySelectorAll('img[src*="/i/t/"]').forEach(
        img => {
            let original = img.src;
            let replacement = img.parentNode.parentNode.getAttribute('imgsrc');
            img.parentNode.parentNode.onclick = (event) => {
                event.preventDefault();
                if (event.shiftKey) {
                    window.open(img.parentNode.parentNode.href, "_blank");
                } else {
                    img.parentNode.removeAttribute('style');
                    replaceImage(img, replacement, original);
                }
            }
        }
    );

    function replaceImage(node, replacement, original) {
        let image = new Image();
        image.src = replacement;
        node.replaceWith(image);
        image.parentNode.parentNode.onclick = (event) => {
            event.preventDefault();
            if (event.shiftKey) {
                window.open(image.parentNode.parentNode.href, "_blank");
            } else {
                image.parentNode.removeAttribute('style');
                replaceImage(image, original, replacement);
            }
        }
    }
}