
function microsoftSam() {
    window.addEventListener('load', () => {
        if (document.querySelector("h2 a[href='/topics/Microsoft_Sam']")) {
            let content = [];
            document.querySelectorAll('.message-container').forEach(node => {
                let poster = node.querySelector('.message-top').children[1].textContent;
                let post = node.querySelector('.message').textContent;
                post = post.substring(0, post.lastIndexOf('---\n'));
                if (post.trim().length == 0) {
                    content.push(`${poster} only blank-posted.`);
                } else {
                    content.push(`${poster} says: ${post}`)
                }
            });
            if (content.length > 0) {
                speak(content);
            }
        }

        function speak(content) {
            let message = content.shift();
            let utter = new SpeechSynthesisUtterance(message);
            utter.voice = window.speechSynthesis.getVoices()[0];
            window.speechSynthesis.speak(utter);
            utter.onend = () => {
                if (content.length > 0) {
                    speak(content)
                }
            }
        }
    });
}