function enable_paste_upload() {
    let parser = new DOMParser();
    let canvas = document.querySelector('div.quickpost-canvas') ?? document.querySelector('form[action="postmsg.php"]');;
    let textarea = canvas.querySelector('textarea');
    
    function handle_upload(event) {
        for (let file of event.clipboardData.files) {
            upload(textarea, file, file.name)
        }
    }

    function upload(textarea, data, filename) {
        let formData = new FormData();
        if (data !== undefined) {
            formData.append('file', data, filename);
        }
        fetchURL(`https://u.endoftheinter.net/u.php`, {
            method: 'POST',
            body: formData,
        }).then(response => {
            let uploader = parser.parseFromString(response, "text/html");
            let img_tag = uploader.querySelector('div.img input[value]').value;
            addTextAreaContent(textarea, img_tag)
        }).error(e => {
            console.error(e)
        })
    }

    function addTextAreaContent(textarea, content) {
        let final_cursor = (textarea.value.substring(0, textarea.selectionStart) + content).length
        textarea.value = textarea.value.substring(0, textarea.selectionStart) + content + textarea.value.substring(textarea.selectionEnd);
        textarea.selectionStart = final_cursor;
        textarea.selectionEnd = final_cursor;
    }

    canvas.addEventListener('paste', handle_upload, false);
}