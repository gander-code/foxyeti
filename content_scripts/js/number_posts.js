function appendPostNumbers(options) {
    let page = new URLSearchParams(window.location.search).get('page') || 1;

    function doNumbering(row, index) {
        let post = (index + 1) + ((page - 1) * 50)
        if (!row.classList.contains('foxyeti-numbered')) {
            row.classList.add('foxyeti-numbered');
            row.appendChild(document.createTextNode(` | #${post}`));
        }
    }
    document.querySelectorAll(options.mobile ? 'section > div:nth-child(1)' : '.message-container>.message-top')
        .forEach((row, index) => doNumbering(row, index))

}