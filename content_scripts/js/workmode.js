function applyWorkmodeToTopicList() {
    filterNWSTopics();
}

function applyWorkModeToMessageList() {
    disableAvatars();
    imagesToLinks();
}

function disableAvatars() {
    document.querySelectorAll('div.userpic-holder').forEach(pic => pic.style.visibility= 'hidden');
}

function imagesToLinks() {
    document.querySelectorAll('.message a[imgsrc], .img-loaded img').forEach((anchor) => {
        let replacement = document.createElement('a');
        replacement.href = anchor.getAttribute('imgsrc');
        replacement.appendChild(document.createTextNode(replacement.href));
        anchor.style.display = 'none';
        anchor.parentNode.appendChild(replacement);
    });
}

function filterNWSTopics() {
    document.querySelectorAll('td.oh').forEach(topic => {
        topic.querySelectorAll('.fr a').forEach(anchor => {
            if (anchor.pathname == "/topics/NWS" || anchor.pathname == "/topics/NLS") {
                topic.parentNode.style.display = 'none';
            }
        })
    })
}