function determineTextColor(bgColor) {
    return isColorBright(bgColor) ? '#000' : '#FFF';
}

function isColorBright(color) {
    let rgb = parseColor(color);
    return (((rgb.R * 255 * 0.299) + (rgb.G * 255 * 0.587) + (rgb.B * 255 * 0.114)) > 186);
}

function parseHexColor(color) {
    var r = parseInt(color.substring(0, 2), 16); // hexToR
    var g = parseInt(color.substring(2, 4), 16); // hexToG
    var b = parseInt(color.substring(4, 6), 16); // hexToB
    return { R: r / 255, G: g / 255, B: b / 255, A: 1.0 };
}

function parseRGBColor(color) {
    if (color.indexOf("rgb(") === 0) {
        color = color.substring("rgb(".length);
    }
    if (color.indexOf(")") === color.length - 1) {
        color = color.substring(0, color.length - 1)
    }
    let rgb = color.split(",");
    return { R: rgb[0] / 255, G: rgb[1] / 255, B: rgb[2] / 255, A: 1.0 }
}

function parseColor(color) {
    return (color.charAt(0) === "#") ? parseHexColor(color) : parseRGBColor(color);
}

function overlay(fg, bg) {
    let r = {};
    r.A = 1;
    r.R = (fg.R * fg.A ) + (bg.R * bg.A * (1 - fg.A));
    r.G = (fg.G * fg.A ) + (bg.G * bg.A * (1 - fg.A));
    r.B = (fg.B * fg.A ) + (bg.B * bg.A * (1 - fg.A));
    return r;
}

function brighten(color, value) {
    let bg = parseColor(color);
    let fg = value > 1 ? { R: 1.0, G: 1.0, B: 1.0, A: value-1.0 } : { R: 0, G: 0, B: 0, A: 1.0-value };
    let rgb = overlay(fg, bg)
    return `rgba(${rgb.R * 255}, ${rgb.G * 255}, ${rgb.B * 255}, ${rgb.A})`;
}