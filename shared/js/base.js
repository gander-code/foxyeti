function customSigStrip(range, message) {
    var sig,
        lines = 0;
    $A(message.childNodes).reverse().forEach(function (ii) {
        if (ii.classList && ii.classList.contains('foxyeti-faded-sig')) {
            sig = ii;
            lines = 3;
        }
        if (lines < 3) {
            if (ii.tagName == 'BR') {
                ++lines
            } else {
                if (ii.nodeType == 3 && ii.nodeValue.replace(/^\s+|\s+$/g, '') == '---') {
                    if (ii.previousSibling && ii.previousSibling.tagName == 'BR') {
                        sig = ii.previousSibling
                    } else {
                        sig = ii
                    }
                }
            }
        }
    });
    var tmp = getNearestContent(range.endContainer, range.endOffset, range.startContainer, range.startOffset, true, sig);
    if (tmp === undefined) {
        return range
    } else if (tmp === false) {
        return false
    }
    var ret = {
        startContainer: range.startContainer,
        startOffset: range.startOffset,
        endContainer: tmp[0],
        endOffset: tmp[1]
    };
    range.detach && range.detach();
    return ret
}