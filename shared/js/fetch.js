function fetchURL(url, options, fetchAs = (response) => response.text()) {
    return fetch(url, { ...options, timeout: 2000 }).then(response => {
        if (!response.ok) {
            throw new Error(`HTTP error! status: ${response.status}`);
        }
        return response;
    }).then(response => {
        return fetchAs(response);
    }).catch(error => {
        console.error(error);
    })
}

function streamFetchURL(url, options, callback = () => { },) {
    fetch(url, { ...options, timeout: 2000 })
        .then(response => response.body)
        .then(rb => {
            const reader = rb.getReader();
            const decoder = new TextDecoder();
            function readStream() {
                reader.read().then(({ done, value }) => {
                    if (done || callback(decoder.decode(value))) {
                        reader.cancel();
                        return
                    }
                    readStream();
                })
            }
            readStream()
        })
}