let feature_dependencies = new Map();
function afterCompletion(event, callback) {
    if (!feature_dependencies.has(event)) {
        feature_dependencies.set(event, []);
    }
    feature_dependencies.get(event).push(callback)
}

function onFeatureComplete(event) {
    if (!feature_dependencies.has(event)) {
        feature_dependencies.set(event, []);
    }
    let callbacks = feature_dependencies.get(event)
    callbacks.forEach(callback => callback)
}

function doIfEnabled(feature, options, ifEnabled, ifNotEnabled = (feature, options) => { }) {
    getSingletonFromStorage('enabledFeatures').then(
        (features) => {
            if (features.enabledFeatures[feature] === true) {
                ifEnabled(options);
            } else {
                ifNotEnabled(feature, options);
                onFeatureComplete(feature)
            }
        },
        onError
    );
}

function doOnceIfEnabled(feature, options, ifEnabled, ifNotEnabled = (feature, options) => { }) {
    if (options.navigation) {
        onFeatureComplete(feature);
        return
    }
    getSingletonFromStorage('enabledFeatures').then(
        (features) => {
            if (features.enabledFeatures[feature] === true) {
                ifEnabled(options);
            } else {
                ifNotEnabled(feature, options);
                onFeatureComplete(feature)
            }
        },
        onError
    );
}

function doOnDesktopIfEnabled(feature, options, ifEnabled, ifNotEnabled = (feature, options) => onFeatureComplete(feature)) {
    if (options.mobile) {
        ifNotEnabled(feature, options)
        return
    }
    doIfEnabled(feature, options, ifEnabled, ifNotEnabled)
}

function getStorageValue(feature) {
    return getSingletonFromStorage('enabledFeatures').then(features => {
        return features.enabledFeatures[feature]
    })
}

function getEnabledValue(feature, ifEnabled, ifNotEnabled = () => { }) {
    getSingletonFromStorage('enabledFeatures').then(features => {
        if (features.enabledFeatures[feature]) {
            ifEnabled(features.enabledFeatures[feature]);
        } else {
            ifNotEnabled();
        }
    });
}

function setFeature(feature, value) {
    getSingletonFromStorage('enabledFeatures').then(
        (features) => {
            features.enabledFeatures[feature] = value;
            enabledFeatures = features;
            browser.storage.local.set(features);
            browser.runtime.sendMessage({
                type: 'feature-set',
                title: feature,
                message: value,
            });
        },
        onError
    );
}

function clearFeatures() {
    browser.storage.local.remove('enabledFeatures').then(
        () => console.log('cleared'),
        onError
    )
}

function onError(error) {
    console.log(`Error: ${error}`);
}  
