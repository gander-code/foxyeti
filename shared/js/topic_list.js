function get_topic_ids() {
    let ids = []
    document.querySelectorAll('.grid tr').forEach(row => {
        let id = getTopicId(row);
        if (id !== null) {
            ids.push(id);
        }
    });
    return ids;
}

function getTopicId(row) {
    let anchor = row.querySelector('.oh .fl a');
    if (anchor == null) {
        return null;
    }
    return anchor.href.substring(anchor.href.indexOf("topic=") + "topic=".length)
}

function getTopicName(row) {
    let anchor = row.querySelector('.oh .fl a');
    if (!anchor) {
        return
    }
    return anchor.textContent;
}

function setTopicName(row, name) {
    let anchor = row.querySelector('.oh .fl a');
    if (!anchor) {
        return
    }
    anchor.textContent = name;
}

function getTCId(row) {
    let td = row.querySelector('td:not(.oh)');
    if (td.children.length == 0) {
        return -1;
    }
    let poster = td.querySelector('a[href*="user="]')
    return td.firstChild.href.substring(poster.href.indexOf("user=") + "user=".length);
}

function getTopicNumMessages(row) {
    let td = row.querySelector('td + td + td');
    if (!td) {
        return
    }
    let total = td.textContent;
    if (isNaN(total)) {
        total = total.substring(0, total.indexOf(' '));
        if (isNaN(total)) {
            return
        }
    }
    return total
}