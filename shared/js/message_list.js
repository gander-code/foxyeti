function getPoster(node, options) {
    if (options && options.mobile) {
        return node.querySelector('div span')
    }
    return node.querySelector('a[href*="profile.php"]');
}

function getUserId(node) {
    let poster = getPoster(node)
    if (poster === null) {
        return 0;
    }
    return poster.href.substring(poster.href.indexOf("user=") + "user=".length);
}

function getTopicIdFromURL() {
    const params = new URLSearchParams(window.location.search);
    return params.get('topic');
}

function isBelowSig(node) {
    let message = node.parentNode;
    while(!message.classList.contains("message") && !message.tagName == "SECTION") {
        message = message.parentNode;
        if (message == null) {
            return
        }
    }
    const sigIndex = indexOfSigNode(message);
    const nodeIndex = new Array(...message.childNodes).indexOf(node);
    return sigIndex !== -1 && nodeIndex >= sigIndex;
}

function indexOfSigNode(node) {
    let sig;
    let length = 0;
    new Array(...node.childNodes).slice().reverse().forEach(child => {
        if (child.classList && child.classList.contains('foxyeti-faded-sig')) {
            sig = child;
            length = 3;
        }
        if (length < 3) {
            if (child.tagName == 'BR') {
                ++length
            } else {
                if (child.nodeType == 3 && child.nodeValue.replace(/^\s+|\s+$/g, '') == '---') {
                    if (child.previousSibling && child.previousSibling.tagName == 'BR') {
                        sig = child.previousSibling
                    } else {
                        sig = child
                    }
                }
            }
        }
    });
  	return sig ? new Array(...node.childNodes).indexOf(sig) : -1;
}