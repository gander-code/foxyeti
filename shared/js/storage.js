var singletons = {};
function getSingletonFromStorage(key) {
    if (!singletons[key]) {
        return browser.storage.local.get(key).then(
            (features) => {
                if (Object.keys(features).length === 0 || features[key] === null) {
                    features[key] = {};
                }
                singletons[key] = features;
                return singletons[key];
            }
        );
    }
    return new Promise((resolve, reject) => {
        resolve(singletons[key])
    });
}

function syncSingleton(key, value, onSync = () => {}) {
    getSingletonFromStorage(key).then((features) => {
        singletons[key] = value;
        browser.storage.local.set(singletons[key]).then(onSync);
    });
}

function clearSingleton(key, callback = () => {}) {
    getSingletonFromStorage(key).then((features) => {
        features[key] = {};
        singletons[key] = features;
        syncSingleton(key, features, callback)
    });
}

function removeFromSingleton(key, entry, callback = () => {}) {
    getSingletonFromStorage(key).then((features) => {
        delete features[key][entry];
        singletons[key] = features;
        syncSingleton(key, features, callback);
    });
}
